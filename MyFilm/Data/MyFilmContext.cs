﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MyFilm.Models
{
    public class MyFilmContext : DbContext
    {
        public MyFilmContext (DbContextOptions<MyFilmContext> options)
            : base(options)
        {
        }

        public DbSet<MyFilm.Models.Film> Film { get; set; }
    }
}
